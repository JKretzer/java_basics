/**
 * Copyright (c) 2020-2024 Sven Gothel
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package sic.test;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

/**
 * Siehe Java Grundelemente, JUC2 01.04 Java01.
 * <p>
 * Basic algorithmen part 2, reverse engineering of given undocumented code.
 * </p>
 */
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class Test0104Java01Aufgabe01b {
    
    static int keine_ahnung_01(final int n) {
        int r = 0;
        int i = n;
        // n = 3
        // r = 0
        // i = n = 3
        // 1: 0 < i -> 0 < 3 ? -> true
        // 1: r = r - 1 = 0 - 1 = -1
        //
        // 2: 0 < i -> 0 < 3 ? -> true
        //
        // for-ever loop -> programmierfehler
        // 
        // 1: 0 < i -> 0 < 3 ? -> true, i=2
        // 1: r = r - 1 = 0 - 1 = -1
        //
        // 2: 0 < i -> 0 < 2 ? -> true, i=1
        // 2: r = r - 1 = -1 - 1 = -2
        //
        // 3: 0 < i -> 0 < 1 ? -> true, i=0
        // 3: r = r - 1 = -2 - 1 = -3
        //
        // 3: 0 < i -> 0 < 0 ? -> false, i=-1 XXXX
        while( 0 < i-- ) {
            r = r - 1;
        }
        // n>=0: return -n; ^^^
        //
        // n<0: return 0;
        //
        // n = -1
        // r = 0
        // i = n = -1
        // 1: 0 < i -> 0 < -1 ? -> false, i=-2 XXXX
        // 
        return r;
    }
    static int negiere_positiv(final int n) {        
        if( n >= 0 ) {
            return -n;
        } else {
            return 0;
        }
    }
    
    @Test
    void test01() {
        // Assertions.assertEquals( 0, keine_ahnung_01(0)); // ??
    }
    

    static int keine_ahnung_02(final int a, int b) {
        // a = 5
        // b = 2       
        int r = 0;
        int i = a;
        // i = 5
        // 1: b < i-- -> 2 < 5 = true, i=4
        // 1: r = 1
        //
        // 2: b < i-- -> 2 < 4 = true, i=3
        // 2: r = 2
        //
        // 3: b < i-- -> 2 < 3 = true, i=2
        // 3: r = 3
        //
        // 3: b < i-- -> 2 < 2 = false, i=2 XXXX
        // return 3;
        //
        // -inf ... -10 ..  -5 ... 0   ... 5  ... 10 ... inf        
        //
        // a>=b: return b-a;  
        // a<b: return 0;
        while( b < i-- ) {
            ++r;
        }
        return r;
    }
    @Test
    void test02() {
        // Assertions.assertEquals( 0, keine_ahnung_02(0)); // ??
    }

    static int keine_ahnung_03(final int a) {
        // 
        // Inspiration: factorial(5) = 1*2*3*4*5, 4 '*' operation mit 5 argumenten
        // Inspiration: factorial(3) = 1*2*3,     2 '*' operation mit 3 argumenten
        //
        // a = 3
        int r = a;
        int i = a;
        // r = 3
        // i = 3
        //
        // 1: r =  3 * 3 =  9, i=2
        // 2: r =  9 * 3 = 27, i=1
        // 3: r = 27 * 3 = 81, i=0 XXX
        //
        // 1: r =  3 * 3                  =  9, i=2
        // 2: r =  3 * 3 * 3     =  9 * 3 = 27, i=1
        // 3: r =  3 * 3 * 3 * 3 = 27 * 3 = 81, i=0 XXX
        
        while( 0 < i-- ) {
            r *= a; // r = r * a;
        }
        // return r=a
        return r;
    }
    // FIXED
    static int factorial(final int a) {
        // 
        // factorial(5) = 1*2*3*4*5, 4 '*' operation mit 5 argumenten
        // factorial(3) = 1*2*3, 2 '*' operation mit 3 argumenten
        //
        int r = a;
        int i = a;
        
        while( 1 < --i ) {
            r *= i; // r = r * a;
        }
        return r;
    }
    @Test
    void test03() {
        // Assertions.assertEquals( 0, keine_ahnung_03(0)); // ??
        Assertions.assertEquals( 1,       factorial(1));
        Assertions.assertEquals( 1*2,     factorial(2));
        Assertions.assertEquals( 1*2*3,   factorial(3));
        Assertions.assertEquals( 1*2*3*4, factorial(4));
    }
    
    // Berechne Epsilon! 
    static float calcEpsilon32() {
        long iter_count = 0;
        float x = 1.0f;
        float r; // rueckgabewert des vorgaengers von x, 1 schleifendurchlauf bevor 1+x==1 -> epsilon (kleinste float wert, welcher noch einen unterschied macht)
        do {
            r = x; 
            x /= 2.0f; // x = x / 2.0f;
            ++iter_count;
        } while( 1.0f + x != 1.0f );
        // 1 + x mit x>0 immer > 1 ?? -> NEIN, denn wir benutzen: 32-bit IEC 559/IEEE 754 float
        // 1.0f + x == 1.0f !!!
        
        System.out.printf("Epsilon 32bit: %d iterations, epsilon %.40f, indifferent %.40f%n", iter_count, r, x);
        // Epsilon 32bit: 24 iterations, epsilon 0.0000001192092895507812500000000000000000, indifferent 0.0000000596046447753906250000000000000000
        return r;
    }
    static double calcEpsilon64() {
        long iter_count = 0;
        double x = 1.0;
        double r; // rueckgabewert des vorgaengers von x, 1 schleifendurchlauf bevor 1+x==1 -> epsilon (kleinste float wert, welcher noch einen unterschied macht)
        do {
            r = x; 
            x /= 2.0; // x = x / 2.0;
            ++iter_count;
        } while( 1.0 + x != 1.0 );
        // 1 + x mit x>0 immer > 1 ?? -> NEIN, denn wir benutzen: 64-bit IEC 559/IEEE 754 float
        // 1.0f + x == 1.0f !!!
        
        System.out.printf("Epsilon 64bit: %d iterations, epsilon %.40f, indifferent %.40f%n", iter_count, r, x);
        // Epsilon 64bit: 53 iterations, epsilon 0.0000000000000002220446049250313000000000, indifferent 0.0000000000000001110223024625156500000000
        return r;
    }
    
    /**
     * 3.3.2 (Static) Globale (Klassen) immutable Variablen, welche mit dem rueckgabewert der static (globalen) methode calcEpsilon32() initialisiert wird.
     * @see {@link Test0104Java01#test03_3_storage_class()}
     */
    static final float Epsilon32 = calcEpsilon32();
    
    static final double Epsilon64 = calcEpsilon64();
    
    /**
     * Returns true if arguments are equal
     * @param a int arg 1
     * @param b int arg 2
     */
    static boolean equals0_ok(int a, int b) {
        // a == b
        return a == b; // OK        
        // a - b == 0
        // return a - b == 0; // Richtig aber ein bisschen bloed (zu aufwaendig)
    }
    
    /**
     * Returns true if arguments are equal
     * @param a float arg 1
     * @param b float arg 2
     */
    static boolean equals1_ok(float a, float b) {
        // return a == b; // code review: rejected due to epsilon (abgelehnt wegen epsilon)
        // a - b == 0
        // return a - b == 0; // Nope -> epsilon
        
        // Wenn die Differenz von a und b kleiner ist als epsilon, 
        // dann sollen die beiden zahlen als gleich interpretiert (angesehen) werden.
        final float epsilon = Epsilon32; // calcEpsilon32()
        final float dAB = Math.abs( a - b ); // die Differenz von `a` und `b`        
        //
        //               epsilon 0.0000001192092895507812500000000000000000 (positiv)
        //
        //               a=-7  b=-6 -> -7-(-6) = -7+(-1)*(-6) = -7+6 = -1 -> -1 < epsilon (!!!) _aber_ abs(-1) > epsilon
        //                      U: dAB = -1, dAB < epsilon, abs(dAB) > epsilon
        //
        //               a=-7  b=-6.999999990801 -> x < 0, x < epsilon, abs(x) < epsilon
        //                      V: dAB = -0.000000019, dAB < epsilon, abs(dAB) < epsilon
        //         
        // -inf ... -10 ..  -5 ... 0   ... 5  ... 10 ... inf
        //                         EE
        //        UUUUU                       abs(UUUUU)
        //           V
        //
        // 
        System.out.printf("equals1 %f, %f -> %b, d %.30f%n", a, b, dAB < epsilon, dAB);
        return dAB < epsilon; // `dAB` kleiner ist als `epsilon`    
    }
    /**
     * Returns true if arguments are equal
     * @param a float arg 1
     * @param b float arg 2
     */
    static boolean equals2_error(float a, float b) {
        final float epsilon = Epsilon32; // calcEpsilon32();
        final float dAB = a - b;
        System.out.printf("equals2 %f, %f -> %b, d %.30f%n", a, b, dAB < epsilon, dAB);
        return dAB < epsilon;
    }
    static void equals12(float a, float b) {
        equals1_ok(a, b);
        equals2_error(a, b);
    }
    @Test
    void test04() {        
        // Weil es ein epsilon fuer float und double gibt:
        // - Mache keinen direkten Vergleich
        //   -> nutze float nicht i.d. schleifen bedingung mit '=='
        //
        // Zusaetzlich:
        // - float/double ist langsamer als ganzzahlen (int, ...) -> CPU Architektur
        //
        equals12(3.0f, 3.0f);   // gleich
        equals12(-3.0f, -3.0f); // gleich
        /**
            equals1 3.000000, 3.000000 -> true, d 0.000000000000000000000000000000
            equals2 3.000000, 3.000000 -> true, d 0.000000000000000000000000000000
            equals1 -3.000000, -3.000000 -> true, d 0.000000000000000000000000000000
            equals2 -3.000000, -3.000000 -> true, d 0.000000000000000000000000000000
         */        
        equals12(3.0f, 1.0f);   // ungleich
        equals12(-3.0f, -2.0f); // ungleich
        /**
            equals1 3.000000, 1.000000 -> false, d 2.000000000000000000000000000000
            equals2 3.000000, 1.000000 -> false, d 2.000000000000000000000000000000
            equals1 -3.000000, -2.000000 -> false, d 1.000000000000000000000000000000
            equals2 -3.000000, -2.000000 -> true, d -1.000000000000000000000000000000 < epsilon (!!!)
            */

    }
    
    static int keine_ahnung_10(final int x) {
        return (0 < x ? 1 : 0) - (x < 0 ? 1 : 0);
    }
    public static int keine_ahnung_11(final int x) {
        return Integer.MIN_VALUE == x ? Integer.MAX_VALUE  : -x;
    }
    public static int keine_ahnung_12(final int x) {
        return keine_ahnung_10(x) < 0 ? keine_ahnung_11( x ) : x;
    }

}
